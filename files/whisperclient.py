#!/usr/bin/python3

from datetime import datetime
import numpy as np
import base64
import requests as rq
import json

class WhisperTimeoutException(Exception):
  pass

class EmptyAudioException(Exception):
  pass

class TranscriptionFailureException(Exception):
  pass

class WhisperClient:

  def __init__(self, api_token, verbose=False):
    self._api_token = api_token
    self._headers = {"X-API-KEY": api_token, "Content-Type": "application/json"}
    self._willma_base_url = "https://willma.liza.surf.nl/api/v0"
    models = rq.get(f"{self._willma_base_url}/models", headers=self._headers).json()
    whisper_model = next(filter(lambda x: "whisper" in x.get("name").lower(), models))
    self._whisper_model_id = whisper_model.get("id")
    self._verbose = verbose

  def transcribe(self, source_wav_filepath, source_name):
    if self._verbose:
      print('Transcribing ...', source_wav_filepath)

    whisper_response = self.get_whisper_response(str(source_wav_filepath))

    if whisper_response:
      return {
        "json": json.dumps(whisper_response),
        "text": self.transcription_to_plain_text(whisper_response, source_name)
      }
    else:
      return {
        "json": '{"message":"Whisper transcription failed."}',
        "text": 'Whisper transcription failed.'
      }

  def transcription_to_plain_text(self, whisper_response, source_name):
    segments = whisper_response['message']['segments']
    duration = float(segments[-1]['end'])
  
    text = f"naam: {source_name}\n"
    text += f"duur: {duration}\n"
    for seg in segments:
      text += "\n"
      text += f"[{self.format_abs_time(seg['start'])}]\n"
      text += f"{seg['text'].lstrip()}\n"
    return text
  
  def get_whisper_response(self, wav_file_path):
    if self._verbose:
      print('Reading', wav_file_path)

    with open(wav_file_path, 'rb') as audio_file:
      b64encoded = base64.b64encode(audio_file.read())

      if len(b64encoded) == 0:
        raise EmptyAudioException(f"EMPTY AUDIO IN {wav_file_path}")
      
      b64audio = b64encoded.decode()
      
    rq_url = f"{self._willma_base_url}/audio/transcriptions"
    rq_data = json.dumps({
        "input": b64audio,
        "sequence_id": self._whisper_model_id,
        "stream": True,
        # "wallet_id": self._wallet_id,
        # "word_timestamps": True,
      })
    
    posted_at = datetime.now()
    if self._verbose:
      print('Posting')

    whisper_response = None
    cumulate_log_output = ''
    with rq.post(rq_url, data=rq_data, headers=self._headers, stream=True) as rsp:
      rsp.raise_for_status()
      shown_seconds = 10
      for rsp_line in rsp.iter_lines():
        if whisper_response:
          # "Read/Discard" last response line and finish loop when the content has already been received.
          continue
        running_for_seconds = (datetime.now() - posted_at).total_seconds()
        shown_seconds_next = running_for_seconds//10
        if self._verbose and shown_seconds < shown_seconds_next:
          shown_seconds = shown_seconds_next
          print('request pending for', running_for_seconds)
        if running_for_seconds > 3600:
          raise WhisperTimeoutException(f"Transcription request abandoned after {running_for_seconds} seconds. Accumulated response so far: \n{cumulate_log_output}")

        partial_response = rsp_line.decode("utf-8")
        if not partial_response.startswith("data:"):
          cumulate_log_output += partial_response
          continue

        response_json = partial_response[len("data:"):].strip()
        rsp_data = json.loads(response_json)

        if rsp_data["data_type"] == "done":
          whisper_response = rsp_data["response"]

    if not whisper_response:
      raise TranscriptionFailureException(
        f"NON-TRANSCRIPTION RESPONSE:\n{cumulate_log_output}"
      )
    
    return whisper_response

  def rolling_mean(self, data, window_size):
    rolling_avg = np.convolve(data, np.ones(window_size)/window_size, mode='same')
    return rolling_avg
  
  def format_abs_time(self, total_seconds):
    ts_int = int(total_seconds)
    abs_sec = ts_int % 60
    abs_min = ts_int // 60
    abs_hr = ts_int // 3600
    return f"{abs_hr:02}:{abs_min:02}:{abs_sec:02}"

  