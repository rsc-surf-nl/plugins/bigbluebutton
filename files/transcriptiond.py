#!/usr/bin/python3

import time
import traceback
from pathlib import Path
import os
import shutil
import xml.etree.ElementTree as etr
import re
from ffmpeg import FFmpeg
from whisperclient import WhisperClient, EmptyAudioException
import yaml
import json
import asyncio

cfg = None
verbose = False
whisper_client = None

class WhisperAvailabilityException(Exception):
  pass

def get_meeting_name(meta_path):
  # Parse name from xml metadata
  tree = etr.parse(meta_path.absolute())
  raw_meeting_name = ( tree
                      .getroot()
                      .find('meta/meetingName')
                      .text
                      .lower() )
  # Sanitize name
  wspace = re.compile('\s+')
  raw_meeting_name = wspace.sub('_', raw_meeting_name)
  schar = re.compile('\W')
  sanitized_meeting_name = schar.sub('', raw_meeting_name)

  # Parse timestamp from xml metadata
  start_time_epoch = ( tree
                      .getroot()
                      .find('start_time')
                      .text )
  start_time = time.localtime((float(start_time_epoch)/1000))
  timestamp = time.strftime('%Y%m%d%H%M%S', start_time)
  return f"{sanitized_meeting_name}_{timestamp}"

def convert_audio_file(video_source_path, audio_source_filepath):
  to_sample_rate = '16000'

  conversion = (
    FFmpeg()
    .option('y')
    .input(video_source_path)
    .output(audio_source_filepath, **{'ar': to_sample_rate, 'ac': 1})
  )
  conversion.execute()

def find_and_process_recordings():
  global cfg
  global whisper_client

  source_parent_dir = Path(cfg['source_parent_dir'])
  done_parent_dir = Path(cfg['done_parent_dir'])
  raw_parent_dir = Path(cfg['raw_parent_dir'])

  if not source_parent_dir.exists():
    return
  
  for meeting_id_path in source_parent_dir.iterdir():
    if not meeting_id_path.is_dir():
      if verbose:
        print(meeting_id_path, 'is not a directory')  
      continue
    meta_path = meeting_id_path.joinpath('metadata.xml')
    if not meta_path.exists():
      if verbose:
        print('No metadata file', meta_path)  
      continue

    video_source_path = meeting_id_path.joinpath('video/webcams.mp4')
    if not video_source_path.exists():
      if verbose:
        print('No video file', video_source_path)
      continue

    meeting_name = get_meeting_name(meta_path)
    done_dir = done_parent_dir.joinpath(meeting_name)
    done_dir.mkdir(parents=True, exist_ok=True)

    video_done = done_dir.joinpath("video.done")
    transcription_request_done = done_dir.joinpath("transcription-request.done")
    json_transcription_done = done_dir.joinpath("json-transcription.done")
    plain_text_transcription_done = done_dir.joinpath("plain-text-transcription.done")
    cleanup_done = done_dir.joinpath("cleanup.done")

    raw_target_dir = raw_parent_dir.joinpath(meeting_name)
    os.makedirs(raw_target_dir, exist_ok=True)

    if not video_done.exists():
      try:
        # copy the interview's video file to Research Drive
        video_target_path = raw_target_dir.joinpath(f"{meeting_name}.mp4")
        if verbose:
          print('Copy video file', video_source_path, 'to', video_target_path)
        shutil.copy(str(video_source_path), str(video_target_path))
        # Set done-token
        os.mknod(str(video_done))
      except Exception as exc:
        traceback.print_exc()
        continue

    transcription_text_path = done_dir.joinpath(f"{meeting_name}.txt")
    transcription_json_path = done_dir.joinpath(f"{meeting_name}.json")
    audio_source_filepath = done_dir.joinpath('source.wav')

    if (
      video_done.exists()
      and not plain_text_transcription_done.exists()
      and not transcription_request_done.exists()
    ):
      try:
        # prevent multiple requests being sent
        os.mknod(str(transcription_request_done))

        if verbose:
          print('Convert audio file into', audio_source_filepath)
        convert_audio_file(video_source_path, audio_source_filepath)

        if not whisper_client:
          raise WhisperAvailabilityException("Whisper client could not be initialized. Restart transcriptiond.service to pick up transcription task again.")

        asyncio.run(
         trigger_transcription(
           meeting_name, json_transcription_done, plain_text_transcription_done,
           raw_target_dir, transcription_text_path, transcription_json_path, audio_source_filepath)
        )
        
      except Exception as exc:
        traceback.print_exc()
        continue
      finally:
        # Now either we have a successful transcription_done token or we need a new request.
        # So we remove this "do not request again" token.
        cleanup_temp_file(transcription_request_done)


    if plain_text_transcription_done.exists() and not cleanup_done.exists():
      try:
        # Remove the temp files in the done directory.
        cleanup_temp_file(transcription_json_path)
        cleanup_temp_file(transcription_text_path)
        cleanup_temp_file(audio_source_filepath)
        cleanup_temp_file(video_source_path)
        # Set done-token
        os.mknod(str(cleanup_done))
      except Exception as exc:
        traceback.print_exc()

async def trigger_transcription(meeting_name, json_transcription_done, plain_text_transcription_done, raw_target_dir, transcription_text_path, transcription_json_path, audio_source_filepath):
    error_txt_path = raw_target_dir.joinpath("error-message.txt") 
    try:
      transcriptions = whisper_client.transcribe(audio_source_filepath, meeting_name)
      whisper_json = transcriptions['json']
      plain_text = transcriptions['text']
    except EmptyAudioException as exc:
      exc_text = traceback.format_exc()
      whisper_json = json.dumps({"message":exc_text}, indent=2)
      plain_text = f"TRANSCRIIPTION ERROR: {exc_text}"
      # Just log and finish task, no point in re-trying
      print(exc_text)
    except Exception as exc:
      exc_text = traceback.format_exc()
      with open(error_txt_path, 'w') as error_file:
        error_file.write(f"Temporary error. Retrying.\n\n{exc_text}")
      raise

    # write transcription files
    with open(transcription_json_path, 'w') as transcription_json_file:
      if verbose:
        print('Writing json transcription file', transcription_json_path)
      transcription_json_file.write(whisper_json)

    with open(transcription_text_path, 'w') as transcription_text_file:
      if verbose:
        print('Writing plain text transcription file', transcription_text_path)
      transcription_text_file.write(plain_text)

    # Now that final responses have been written,
    # the retryable error file can be removed.
    cleanup_temp_file(error_txt_path)

    # copy the transcription files to Research Drive
    transcription_json_target_path = raw_target_dir.joinpath(f"{meeting_name}.json")
    if verbose:
      print('Writing to', transcription_json_target_path)
    shutil.copy(str(transcription_json_path), str(transcription_json_target_path))
    # Set done-token
    os.mknod(str(json_transcription_done))

    transcription_text_target_path = raw_target_dir.joinpath(f"{meeting_name}.txt")
    if verbose:
      print('Writing to', transcription_text_target_path)
    shutil.copy(str(transcription_text_path), str(transcription_text_target_path))
    # Set done-token
    os.mknod(str(plain_text_transcription_done))

def cleanup_temp_file(temp_path):
  if temp_path.exists():
    if verbose:
      print('Cleaning up', temp_path)
    os.unlink(str(temp_path))


def main():
  try:
    find_and_process_recordings()    
  except Exception as exc:
    traceback.print_exc()


if __name__ == '__main__':

  with open('/usr/local/bigbluebutton/core/scripts/willma_token', 'r') as willma_file:
    willma_token = willma_file.read().strip()

  with open('/usr/local/bigbluebutton/core/scripts/transcriptiond.config.yaml') as config_file:
    cfg = yaml.safe_load(config_file)
    if 'verbose' in cfg:
      verbose = cfg['verbose']
  
  try:
    whisper_client = WhisperClient(willma_token, verbose)
  except Exception as exc:
    whisper_client = None
    traceback.print_exc()
    print("RUNNING WITHOUT WHISPER-TRANSCRIPTION!")

  while True:
    main()
    # every minute
    time.sleep(60)

