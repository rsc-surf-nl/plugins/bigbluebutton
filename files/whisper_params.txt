# params set in whisper:
verbose: Optional[bool] = None,
               temperature: Union[float, tuple[float, ...]] = (0.0, 0.2, 0.4, 0.6, 0.8, 1.0),
               compression_ratio_threshold: Optional[float] = 2.4,
               logprob_threshold: Optional[float] = -1.0,
               no_speech_threshold: Optional[float] = 0.6,
               condition_on_previous_text: bool = True,
               initial_prompt: Optional[str] = None,
               word_timestamps: bool = False,
               prepend_punctuations: str = "\"'“¿([{-",
               append_punctuations: str = "\"'.。,，!！?？:：”)]}、",

?temperature, seek?

# Transcribe an audio file using Whisper
Params:
model – The Whisper model instance
audio – The path to the audio file to open, or the audio waveform
verbose – Whether to display the text being decoded to the console. If True, displays all the details, If False, displays minimal details. If None, does not display anything
temperature – Temperature for sampling. It can be a tuple of temperatures, which will be successively used upon failures according to either `compression_ratio_threshold` or `logprob_threshold`.
compression_ratio_threshold – If the gzip compression ratio is above this value, treat as failed
logprob_threshold – If the average log probability over sampled tokens is below this value, treat as failed
no_speech_threshold – If the no_speech probability is higher than this value AND the average log probability over sampled tokens is below `logprob_threshold`, consider the segment as silent
condition_on_previous_text – if True, the previous output of the model is provided as a prompt for the next window; disabling may make the text inconsistent across windows, but the model becomes less prone to getting stuck in a failure loop, such as repetition looping or timestamps going out of sync.
initial_prompt – Optional text to provide as a prompt for the first window. This can be used to provide, or "prompt-engineer" a context for transcription, e.g. custom vocabularies or proper nouns to make it more likely to predict those word correctly.
word_timestamps – Extract word-level timestamps using the cross-attention pattern and dynamic time warping, and include the timestamps for each word in each segment.
prepend_punctuations – If word_timestamps is True, merge these punctuation symbols with the next word
append_punctuations – If word_timestamps is True, merge these punctuation symbols with the previous word
decode_options – Keyword arguments to construct `DecodingOptions` instances
