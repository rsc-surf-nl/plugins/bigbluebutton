# Record interviews securely with SURF Research Cloud

## In Short
This catalog item of SURF Research Cloud enables the members of a collaborative organization to conduct and record online video calls securely. There is full control on who can conduct interviews and who can watch them, later. The server is a [Big Blue Button](https://bigbluebutton.org) instance. The frontend we use to administrate users, rooms, calls and recordings is Greenlight. Interviews are transcribed by SURF's own Whisper instance (running the Dutch Large Language Model). Videos and transcriptions are saved to Research Drive and can be viewed without downloading. All data remains inside SURF's rescources. SURF's own Whisper instance does not hold on to submitted data in any way. The data is not saved and in particular not used for further training or the model.

## Main document

The overview on the entire SI - Secure Interviews ecossystem [can be found here](https://servicedesk.surf.nl/wiki/x/agCGBw).

Please also be aware of [the WiLLMa Rules of use](https://surfdrive.surf.nl/files/index.php/s/C1FEAvmEjYlZoS3), for the transcription part of the service.

## Starting the interview server
As a [developer in your collaboration](https://servicedesk.surf.nl/wiki/x/AAEQAQ), clone SURF's catalog item "Secure Interviews" and continue with the wizard flow that gets opened. Rename your new catalog item to your liking and especially overwrite parameters in the "Parameters" step if you want to customize the behaviour of the insterview server. 
Start a new workspace with your catalog item.
The hostname you choose will be the first part of the interview-server's URL.

**Make sure to choose a short hostname, shorter than 14 characters.**
Just "interviews" is a perfectly fine name, for instance.

### Manual steps
When the server is running, a few manual steps are required to make the server work as we intend:

#### Log in to the workspace with ssh to create the first admin user.

```
sudo su
cd /root/greenlight-v3
docker exec greenlight-v3 bundle exec rake admin:create['admin','<e-mail>','<password>']
exit
```

The terminal should report back with

```
User account was created successfully!
```

#### Visit the server and sign in as admin

When a user account is present, you can navigate to the Big Blue Button application itself with the yellow "Access" button that is shown on the workspace display in the Research Cloud portal. (You can also bookmark the url and go there directly, later on.)

- Click the big blue "Sign In" button on the top right.
- Use the e-mail and the password you just entered for the first admin user.
- Go to the "Administrator Panel" with the menu on the top right.

#### Change site settings

On the left, choose "Site Settings".
This will take you to the "Customize Greenlight" view.

It is to your liking what you do with the "Appearance" and "Administration" tabs, here.

On the "Settings" tab you set the Default Recording Visibility to "Unpublished". This makes sure that recorded interview content is never shared by this server. It is also necessary for the transcription mechanism and the Research Drive export to work.

On the "Registration" tab, set "Registration Method" to "Join by Invitation" and "Default Role" to "User".

#### Change room configuration

Choose the "Room Configuration" tab of the "Site Settings" view.

**We recommend to apply the following settings:**
- Allow Room to be recorded: "Force Enabled"
- Require users to be signed in before joining: "Disabled"
- Require moderator approval before joining: "Force Enabled"
- Allow any user to start a meeting: "Disabled"
- All users join as moderators: "Disabled"
- Mute users when they join: "Optional (default: disabled)"
- Viewer Access Code: "Force Enabled"
- Moderator Access Code: "Optional (default: disabled)"

All settings with "Optional ..." can be overwritten by the interviewers for their own rooms. The settings not left optional here, are important for a secure and well-structured way of working.

## Create more users

On the left choose "Manage Users". You can now create accounts for other users. At the time of writing the "invite" feature did not work as expected. You can use the "New User" button, though. Provide a temporaty password and instruct the user to change it when they first log in.

The "Full Name" is the name that participants in calls will see.

To conduct interviews, a user should have the "user" role.

People to be interviewed do not require an account.

They can join with the room URL and the code that they receive.

Having at least two users with the "admin" role is advisable.

Admin users can add and remove users and can view all rooms and recordings - even protected ones.

## Resume after pausing server

After pausing and resuming the server you may have to restart the Greenlight frontend.
Log in to the server with ssh and execute the following:

```
sudo bash
cd /root/greenlight-v3
docker-compose up --detach
exit
```

After a few moments the server will be available again through the given URL.



