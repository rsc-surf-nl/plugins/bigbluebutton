## These are a few actions that a maintainer should be able to execute

## Reset the password of a BigBlueButton user

from https://github.com/bigbluebutton/greenlight/issues/1011
look out: Greenlight-v__3__

## Make a backup of the BigBlueButton user database

from https://github.com/bigbluebutton/greenlight/issues/2024
look out: Greenlight-v__3__

or rather
https://shamsherhaider.com/migrating-between-greenlight-v3-servers-in-bigbluebutton/




