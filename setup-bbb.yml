---
- name: Prepare BigBlueButton installation script
  hosts: localhost
  gather_facts: true
  become: true

  tasks:
  - name: Get required packages
    apt:
      name:
      - language-pack-en
      - python3.8-distutils

  - name: Download get-pip
    get_url:
      url: https://bootstrap.pypa.io/get-pip.py
      dest: ./
      mode: '0777'

  - name: Remove stray certbot install
    apt:
      name: certbot
      state: absent

  - name: Activate pip3.8
    shell: >
      python3.8 ./get-pip.py

  - name: Install packages to system python
    shell: >
      python3.8 -m pip install --upgrade
      pyopenssl certbot python-ffmpeg numpy

  - name: Set required locale
    shell: update-locale LANG=en_US.UTF-8
    
  - name: Get installation script
    get_url:
      url: https://raw.githubusercontent.com/bigbluebutton/bbb-install/v2.7.x-release/bbb-install.sh
      dest: /tmp/bbb-install.sh
      mode: 0755

  # e (cert-email), g (install greenlight), j (skip hardware requirements check)
  - name: Run installation script
    async: 1800
    poll: 10
    shell: >
      /tmp/bbb-install.sh
      -s {{ workspace_fqdn }}
      -v focal-270
      -e {{ secure_interviews_cert_email }}
      -g
      -j

  - name: Set recording "always on"
    lineinfile:
      path: /etc/bigbluebutton/bbb-web.properties
      line: |
        autoStartRecording=true
        allowStartStopRecording=false

  - name: Use mp4 format for video
    lineinfile:
      path: /usr/local/bigbluebutton/core/scripts/presentation.yml
      regexp: '- webm'
      line: '  - mp4'

  - name: Copy python files for transcription service
    copy:
      src: transcriptiond.py
      dest: /usr/local/bigbluebutton/core/scripts/transcriptiond.py
      mode: 0755

  - name: Copy whisper client for transcription service
    copy:
      src: whisperclient.py
      dest: /usr/local/bigbluebutton/core/scripts/whisperclient.py
      mode: 0755

  - name: Copy Jupyter notebook for interactive debugging
    copy:
      src: whisper_interactive_test.ipynb
      dest: /usr/local/bigbluebutton/core/scripts/whisper_interactive_test.ipynb
      mode: 0777

  - name: Write config file for transcription service
    copy:
      dest: /usr/local/bigbluebutton/core/scripts/transcriptiond.config.yaml
      mode: 0744
      content: |
        source_parent_dir: /var/bigbluebutton/unpublished/presentation
        done_parent_dir: /var/bigbluebutton/transcription-done
        raw_parent_dir: /data/project/raw
        verbose: False

  - name: Copy transcription service unit file
    copy:
      src: transcriptiond.service
      dest: /etc/systemd/system/transcriptiond.service
      mode: 0644

  - name: Write the willma token to a file to be picked up by the transcription service
    copy:
      content: "{{ secure_interviews_willma_token }}"
      dest: /usr/local/bigbluebutton/core/scripts/willma_token
      mode: 0644

  - name: Enable and start the transcription service
    systemd:
      name: transcriptiond
      state: started
      enabled: true

  - name: Switch off English start message (confuses Dutch LLM)
    command: > 
      mv /opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/{{ item }}/conf-alone.wav
      /opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/{{ item }}/_conf-alone.wav
    loop:
      - "8000"
      - "16000"
      - "32000"
      - "48000"

  - name: Restart BBB
    shell: bbb-conf --restart
